resource "libvirt_ignition" "ignition" {
  name = "${var.instance.name}-ignition"
  content = var.ignition_config
}

resource "libvirt_volume" "coreos" {
  name = "${var.instance.name}-system"
  source = var.instance.image
}

resource "libvirt_volume" "system" {
  name = "${var.instance.name}_system.qcow2"
  base_volume_id = libvirt_volume.coreos.id
}

resource "libvirt_network" "bridge" {
  name = var.instance.network.name
  mode = var.instance.network.mode
  bridge = var.instance.network.bridge
}

resource "libvirt_domain" "vm" {
  coreos_ignition = libvirt_ignition.ignition.id
  name = var.instance.name
  description = var.instance.description
  vcpu = var.instance.vcpu
  memory = var.instance.memory
  running = var.instance.running
  disk {
    volume_id = libvirt_volume.system.id
  }
  autostart = var.instance.autostart
  network_interface {
    network_id = libvirt_network.bridge.id
  }
}
