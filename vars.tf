variable "instance" {
  type = object({
    name = string
    description = optional(string, "Terraform VM")
    vcpu = number
    memory = number
    running = optional(bool, true)
    image = string
    disk_size = optional(number, 12288) # 12GiB
    autostart = optional(bool, true)
    network = object({
      name = string
      mode = optional(string, "bridge")
      bridge = string
      nameservers = list(string)
      addresses = list(string)
    })
  })
}

variable "ignition_config" {
  type = string
}
